import pytest
import shapely
from stratipy import elevation


def test_get_3dep_tile_urls():
    bbox_1 = (-89.9, 40.1, -89.1, 40.9)
    actual_1 = elevation.get_3dep_tile_urls(bbox_1, url_base="")
    expected_1 = ["n41w090/USGS_13_n41w090.tif"]
    assert actual_1 == expected_1

    bbox_2 = (-90.1, 40.1, -89.1, 40.9)
    actual_2 = elevation.get_3dep_tile_urls(bbox_2, url_base="")
    expected_2 = ["n41w091/USGS_13_n41w091.tif", "n41w090/USGS_13_n41w090.tif"]
    assert actual_2 == expected_2

    bbox_4 = (-90.1, 39.9, -89.1, 40.9)
    actual_4 = elevation.get_3dep_tile_urls(bbox_4, url_base="")
    expected_4 = [
        "n40w091/USGS_13_n40w091.tif",
        "n41w091/USGS_13_n41w091.tif",
        "n40w090/USGS_13_n40w090.tif",
        "n41w090/USGS_13_n41w090.tif",
    ]
    assert actual_4 == expected_4
