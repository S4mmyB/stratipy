import pytest
from stratipy import io
import json

@pytest.fixture
def field_boundary():
	return {
		'type': 'Polygon',
		'coordinates': [
			[
				[-84.59305286407469, 39.556934508617076],
				[-84.59270954132079, 39.55203740161884],
				[-84.58326816558838, 39.55213667073393],
				[-84.58386898040771, 39.55607423106763],
				[-84.58438396453857, 39.555644088291956],
				[-84.58524227142334, 39.555644088291956],
				[-84.58549976348876, 39.55676907145539],
				[-84.59305286407469, 39.556934508617076]
			]
		]
	}

@pytest.fixture
def location_coords():
	return [
		(39.55519893168147, -84.58548552017902),
		(39.55246191748659, -84.58383482805323),
		(39.55437420270519, -84.58481585750525),
		(39.5548436670673, -84.5857306022019),
		(39.55547142333218, -84.58559244806948),
		(39.55418066782743, -84.59285266811011),
		(39.55484540868369, -84.59015295498435),
		(39.55260996244743, -84.5866227017214),
		(39.55645927829489, -84.58544179916504),
		(39.55452222646758, -84.58760381087788),
		(39.556200610161966, -84.59033879815297),
		(39.55305455591696, -84.59068045868192),
		(39.55586776916871, -84.58732441446747),
		(39.55334874655486, -84.5918340447357),
		(39.5541772243936, -84.58400804523929),
	]

def test_create_export_file(location_coords, field_boundary):
	actual_string = io.create_export_file(
		location_coords, 
		field_boundary,
		# stratification
	)
	actual = json.loads(actual_string)
	assert len(actual['locationCollections']) == 1
	assert len(actual['areas']) == 1
	assert len(actual['stratifications']) == 1
	assert len(actual['fields']) == 1
	assert len(actual['locations']) == len(location_coords)
	# assert a

# def test_create_area():

# def test_create_stratification():

# def test_create_location_collection():

# def test_create_location():

# def test_create_field():
