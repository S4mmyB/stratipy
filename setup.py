import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="stratipy",
    version="0.5.2",
    author="Will Gardiner, Daniel Kane",
    author_email="gardiner.w@gmail.com",
    license="GPLv3",
    description="Stratify boundaries for sampling",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/S4mmyB/stratipy.",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Scientific/Engineering",
    ],
    python_requires=">=3.7",
    install_requires=[
        "sat_search",
        "intake",
        "intake_stac",
        "shapely",
        "utm",
        "affine",
        "rasterio",
        "fiona",
        "geopandas",
        "landlab",
        "tqdm",
        "pyproj",
        "xarray",
        "aiohttp",
        "pandas",
        "xmltodict",
        "numpy",
        "bson",
    ],
)
