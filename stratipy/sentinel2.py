from multiprocessing import Pool
import satsearch
from stratipy import utils
from stratipy import fetch
import numpy as np
import intake
import intake_stac
import rasterio as rio
from tqdm.auto import tqdm
import re
import asyncio
from collections import OrderedDict, defaultdict
from stratipy.constants import WSG84_CRS
import rasterio.warp as rio_warp


def get_s2_l2a_catalog(bbox, start_date, end_date, max_cloud):
    """
    Parameters
    ----------
    bbox : list
        [xmin, ymin, xmax, ymax]
    start_date : str
        'YYYY-MM-DD'
    end_date : str
        'YYYY-MM-DD'
    max_cloud : int
        max cloud cover <= 100

    Returns
    -------
    stac
        ImageCollection

    """
    URL = "https://earth-search.aws.element84.com/v0"
    dates = "/".join([start_date, end_date])
    cloud_query = {'eo:cloud_cover': {'lt': max_cloud}}

    results = satsearch.Search.search(
        url=URL,
        collections=["sentinel-s2-l2a-cogs"],
        datetime=dates,
        bbox=bbox,
        sort=["<datetime"],
        query=cloud_query,
    )

    items = results.items()
    catalog = intake.open_stac_item_collection(items)
    return catalog

def filter_catalog(catalog, regex):
    return {
        key: catalog[key] for key in list(catalog) if re.match(regex, key)
    }


def get_inputs(catalog, bands=["B04", "B08", "SCL"]):
    return {k: {band: v[band].urlpath for band in bands} for k, v in catalog.items()}


def group_dict_by_scene(dct):
    grouped = defaultdict(list)
    for k in dct:
        scene_id = k.split("_")[1]
        grouped[scene_id].append(k)

    return dict(grouped)


async def fetch_async(inputs, bbox=None, bbox_crs=None):
    flat_ins_dict = {
        f"{item_key}.{band_key}": band_val
        for item_key, item_val in inputs.items()
        for band_key, band_val in item_val.items()
    }

    tasks = [
        asyncio.create_task(
            fetch.fetch_raster_async_wrapper(
                key,
                url,
                bbox=bbox,
                bbox_crs=bbox_crs,
            )
        )
        for key, url in flat_ins_dict.items()
    ]

    flat_results_list = []
    errors = []
    for task in tqdm(tasks, total=len(list(tasks))):
        try:
            flat_results_list.append(await task)
        except rio.errors.RasterioIOError as exc:
            errors.append(exc)

    results = defaultdict(dict)
    for key, result in flat_results_list:
        if type(result) == tuple:  # When would this not be a tuple? for errors?
            item, band = key.split(".")
            results[item][band] = result

    return dict(results), errors



def mask_only_veg_non_veg(r):
    return np.logical_or(r == 4, r == 5)


def get_masked_ndvi(red=None, nir=None, scl=None):
    if red is None or nir is None or scl is None:
        raise ValueError("red, nir, and scl input layer kwargs must be defined")

    # handle divide by zero case, fill with nans, where there would be /0
    # equivalent to ndvi = (nir - red) / (nir + red)
    num = nir - red
    den = nir + red
    out_fill = np.zeros(num.shape)
    out_fill[:] = 0
    ndvi = np.divide(num, den, out=out_fill, where=den != 0)
    scl_mask = mask_only_veg_non_veg(scl)
    masked_ndvi = np.where(scl_mask, ndvi, np.nan)
    return masked_ndvi


def get_max_ndvi_raster(masked_ndvi_rasters):
    return np.nanmax(np.stack(masked_ndvi_rasters), axis=0)

def get_mean_ndvi_raster(masked_ndvi_rasters):
    return np.nanmean(np.stack(masked_ndvi_rasters), axis=0)


def calc_ndvi(item):
    red = item["B04"]
    nir = item["B08"]
    scl = item["SCL"]
    if scl[0].shape != red[0].shape:
        with rio.Env():
            scl_projected, _ = rio_warp.reproject(
                scl[0],
                np.zeros(red[0].shape),
                src_transform=scl[1],
                src_crs=scl[2],
                dst_transform=red[1],
                dst_crs=red[2],
                resampling=rio.enums.Resampling.nearest,
            )
    return get_masked_ndvi(red=red[0], nir=nir[0], scl=scl_projected)


def summarize_ndvi(item_rasters, method):
    MASKED_RATIO_THRESHOLD = 0.3
    NDVI_MAX = 1.0

    # check >= 30% of raster is not masked out & that it's within range of NDVI
    is_good_raster = lambda x: np.logical_and(
        np.count_nonzero(np.isnan(x)) / x.size < MASKED_RATIO_THRESHOLD,
        np.nanmax(x) <= NDVI_MAX,
    )
    ndvi_rasters = [calc_ndvi(item) for item in item_rasters]
    good_ndvi_rasters = filter(is_good_raster, ndvi_rasters)
    if method == 'max':
        return get_max_ndvi_raster(good_ndvi_rasters)
    else:
        return get_mean_ndvi_raster(good_ndvi_rasters)


async def get_ndvi_summary_by_bbox_async(bbox, catalog, summary_method, dst_crs=None):
    items = get_inputs(catalog)
    rasters, errors = await fetch_async(items, bbox=bbox, bbox_crs=WSG84_CRS)

    # 1. groupby scene id
    # 2. summarize ndvi foreach scene id
    # 3. merge summaries

    tile_keys_by_scene = group_dict_by_scene(rasters)
    ndvi_summaries = [
        (
            summarize_ndvi([rasters[key] for key in scene_item_keys], summary_method),
            rasters[scene_item_keys[0]]["B04"][1],
            rasters[scene_item_keys[0]]["B04"][2],
        )
        for scene_item_keys in tile_keys_by_scene.values()
    ]

    if dst_crs is not None:
        return utils.merge_rasters(ndvi_summaries, dst_crs=dst_crs)

    return utils.merge_rasters(ndvi_summaries, dst_crs=ndvi_summaries[0][2])
