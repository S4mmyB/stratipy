import requests
import urllib.request

# import urllib.parse
import fiona
from fiona import Env as fiona_env
import geopandas
import pandas
import json
import shapely.geometry
from stratipy import utils
import pyproj
import rasterio as rio
from stratipy.constants import NAD_CRS
import aiohttp

MAP_UNIT_VECTOR_URL_BASE = "https://sdmdataaccess.nrcs.usda.gov/Spatial/SDMNAD83Geographic.wfs?Service=WFS&Version=1.0.0&Request=GetFeature&Typename=MapunitPoly&BBOX="
SDA_QUERY_URL = "https://sdmdataaccess.sc.egov.usda.gov/tabular/post.rest"


def get_ssurgo_url(bbox):
    return f"{MAP_UNIT_VECTOR_URL_BASE}{','.join(map(str, bbox))}"


async def fetch_map_units_gml(bbox):
    # response = requests.get(get_ssurgo_url(bbox))
    # return response.content
    async with aiohttp.ClientSession() as session:
        async with session.get(get_ssurgo_url(bbox)) as response:
            # return await response.text()
            return await response.read()


async def fetch_map_units(bbox):
    data = await fetch_map_units_gml(bbox)
    reader = fiona.BytesCollection

    with fiona_env():
        with reader(data) as features:
            # return list(features)
            # shapely.geometry.mapping(bbox)
            return list(features)


async def fetch_map_unit_bounds(bbox, dst_crs=None):
    # result = geopandas.read_file(
    #     get_ssurgo_url(bbox),
    #     mask=shapely.geometry.box(*bbox),
    #     crs=NAD_CRS
    # ).set_crs(NAD_CRS)

    data = await fetch_map_units(bbox)
    result = geopandas.GeoDataFrame.from_features(data, crs=NAD_CRS)

    if dst_crs is not None:
        return result.to_crs(dst_crs)

    return result


def get_map_unit_query(bbox):
    polygon = shapely.geometry.box(*bbox, ccw=True)
    query_mu = (
        "SELECT mukey, cokey, compname, comppct_r "
        + "FROM component "
        + "WHERE mukey IN "
        + "(SELECT DISTINCT mukey FROM SDA_Get_Mukey_from_intersection_with_WktWgs84('"
        + polygon.wkt
        + "') ) "
        + "ORDER BY mukey, cokey, comppct_r DESC"
    )
    return query_mu


def fetch_map_unit_data(bbox):
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    query_mu = get_map_unit_query(bbox)
    query_string = f"query={query_mu}&format=JSON%2BCOLUMNNAME"
    sda_mu_response = requests.request(
        "POST", SDA_QUERY_URL, headers=headers, data=query_string
    )
    sda_table_list = json.loads(sda_mu_response.text)["Table"]
    mukey_df = pandas.DataFrame(
        sda_table_list[1:], columns=sda_table_list[0], dtype="object"
    )
    mukey_df = mukey_df.astype({"comppct_r": "float64"})
    return mukey_df


def get_component_query(cokeys):
    cokey_query = (
        "SELECT cokey, chkey, hzname, hzdept_r, hzdepb_r, claytotal_r, om_r "
        + "FROM chorizon "
        + "WHERE cokey IN ("
        + ", ".join(cokeys)
        + ")"
    )
    return cokey_query


def fetch_component_data(cokeys):
    cokey_query = get_component_query(cokeys)
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    query_string = f"query={cokey_query}&format=JSON%2BCOLUMNNAME"

    cokey_response = requests.request(
        "POST", SDA_QUERY_URL, headers=headers, data=query_string
    )
    cokey_table = json.loads(cokey_response.text)["Table"]
    cokey_df = pandas.DataFrame(cokey_table[1:], columns=cokey_table[0])
    return cokey_df.astype(
        {
            "hzdept_r": "float64",
            "hzdepb_r": "float64",
            "claytotal_r": "float64",
            "om_r": "float64",
        }
    )


def summarize_upper_component_data_by_map_unit(component_data, map_unit_data):
    # query component with horizon depth top less than 30cm?
    # I'm confused here, query('hzdept_r<30') returns 70 results, query('hzdept_r>30') returns 72
    # and len(cokey_df) is only 73?
    cokey_df = component_data.query("hzdept_r<30").copy()
    cokey_df["proportion"] = cokey_df.apply(
        lambda row: (row.hzdepb_r - row.hzdept_r) / 30, axis=1
    )

    def wm_om(x):
        names = {
            "wmean_om": (x["proportion"] * x["om_r"]).sum() / x["proportion"].sum(),
            "wmean_clay": (x["proportion"] * x["claytotal_r"]).sum()
            / x["proportion"].sum(),
        }
        return pandas.Series(names, index=["wmean_om", "wmean_clay"])

    cokey_summary = cokey_df.groupby("cokey").apply(wm_om)
    # cokey_summary['cokey'] = cokey_summary.index

    mu_co_join = map_unit_data.join(cokey_summary, how="inner", on="cokey")

    def wm_om2(x):
        names = {
            "wmean_om": (x["comppct_r"] * x["wmean_om"]).sum() / x["comppct_r"].sum(),
            "wmean_clay": (x["comppct_r"] * x["wmean_clay"]).sum()
            / x["comppct_r"].sum(),
        }
        return pandas.Series(names, index=["wmean_om", "wmean_clay"])

    processed_mu_co_join = mu_co_join.groupby("mukey").apply(wm_om2)
    # processed_mu_co_join['mukey'] = processed_mu_co_join.index
    processed_mu_co_join.index = processed_mu_co_join.index.astype("int64")
    return processed_mu_co_join


def clip_map_unit_polygons(mu_polys, boundary):
    # return [boundary.intersection(b) for b in mu_polys_dat['geometry']]
    return geopandas.GeoDataFrame(
        mu_polys, geometry=[boundary.intersection(b) for b in mu_polys["geometry"]]
    )  # .astype({"mukey": "object"})


async def get_clipped_summary(bbox, dst_crs=None):
    # mub = fetch_map_unit_bounds(bbox, dst_crs=dst_crs);
    mub = await fetch_map_unit_bounds(bbox)
    # transformer = pyproj.Transformer.from_crs(
    #     pyproj.CRS(NAD_CRS),
    #     pyproj.CRS(dst_crs),
    #     always_xy=True,
    # )
    # bbox_utm = [
    #     *transformer.transform(*bbox[0:2]),
    #     *transformer.transform(*bbox[2:4]),
    # ]
    map_unit_bounds = clip_map_unit_polygons(
        mub,
        shapely.geometry.box(*bbox),
        # TODO: pass instead
        # shapely.geometry.box(*bbox_utm),
    )
    # print('clipped mub crs', mub.crs)
    map_unit_data = fetch_map_unit_data(bbox)
    component_data = fetch_component_data(map_unit_data["cokey"].unique())
    component_summary = summarize_upper_component_data_by_map_unit(
        component_data, map_unit_data
    )
    component_summary.index = component_summary.index.astype("int64")
    result = map_unit_bounds.astype({"mukey": "int64"}).join(
        # result = map_unit_bounds.join(
        component_summary,
        on="mukey",
        how="left",
    )

    if dst_crs is not None:
        return result.set_crs(NAD_CRS).to_crs(dst_crs)
        # return result.to_crs(dst_crs)

    return result.set_crs(NAD_CRS)


def get_om_clay_rasters(
    df,
    bbox,
    out_shape,
    transform=None,
    bbox_crs=None,
    dst_crs=None,
):
    # Convert bbox to destination CRS if necessary
    if bbox_crs is not None and not pyproj.crs.CRS(bbox_crs).equals(
        pyproj.crs.CRS(dst_crs)
    ):
        bb = utils.reproject_bbox(bbox, bbox_crs, dst_crs)
    else:
        bb = bbox
    
    # Convert results dataframe to destination CRS if necessary
    if dst_crs is not None and not pyproj.crs.CRS(dst_crs).equals(
        pyproj.crs.CRS(df.crs)
    ):
        df = df.to_crs(dst_crs)

    # Generate transform from bbox if not passed
    if transform is not None:
        tf = transform
    else:
        tf = rio.transform.from_bounds(
            west=bb[0],
            south=bb[1],
            east=bb[2],
            north=bb[3],
            width=out_shape[1],
            height=out_shape[0],
        )

    wmean_om = rio.features.rasterize(
        [(row.geometry, row.wmean_om) for _, row in df.iterrows()],
        transform=tf,
        out_shape=out_shape,
    )

    wmean_clay = rio.features.rasterize(
        [(row.geometry, row.wmean_clay) for _, row in df.iterrows()],
        transform=tf,
        out_shape=out_shape,
    )

    return wmean_om, wmean_clay, tf
