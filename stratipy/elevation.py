from math import floor, ceil
import itertools
from landlab import RasterModelGrid, RadialModelGrid
import rasterio as rio
from numpy import zeros, nan
from stratipy.fetch import (
    fetch_raster_async_wrapper,
    fetch_raster_xr_async,
    fetch_raster_async,
)
from stratipy.utils import merge_rasters
from typing import Tuple, List
from stratipy.constants import GDAL_NODATA
import warnings

import aiohttp
import asyncio
import xmltodict

# import rioxarray


def get_3dep_tile_urls(
    bbox: Tuple[float],
    url_base: str = "https://prd-tnm.s3.amazonaws.com/StagedProducts/Elevation/13/TIFF/",
) -> List[str]:
    """Generate 3DEP dataset tile urls from long/lat bounding box
    Only covers US, so it only covers negative longitudes and positive latitudes.

    Parameters:
        bbox: tuple, required
            bounding box (minx, miny, maxx, maxy) in NAD83 CRS
        url_base: string, re

    See reference: https://www.sciencebase.gov/catalog/item/5f7784f982ce1d74e7d6cbd5
    """
    minX, minY, maxX, maxY = bbox
    tiles = [
        f"n{lat}{f'w{str(lon)[1:].zfill(3)}' if lon < 0 else f'e{lon:03}'}"
        for (lon, lat) in itertools.product(
            range(floor(minX), ceil(maxX)), range(floor(minY) + 1, ceil(maxY) + 1)
        )
    ]
    return list(map(lambda x: f"{url_base}{x}/USGS_13_{x}.tif", tiles))


def get_3dep_tiles(
    bbox: Tuple[float],
    url_base: str = "https://prd-tnm.s3.amazonaws.com/StagedProducts/Elevation/13/TIFF/",
) -> List[str]:
    """Generate 3DEP dataset tile and metadata urls from long/lat bounding box
    Only covers US, so it only covers negative longitudes and positive latitudes.

    Parameters:
        bbox: tuple, required
            bounding box (minx, miny, maxx, maxy) in NAD83 CRS
        url_base: string, re

    See reference: https://www.sciencebase.gov/catalog/item/5f7784f982ce1d74e7d6cbd5
    """
    minX, minY, maxX, maxY = bbox
    coord_pairs = itertools.product(
        range(floor(minX), ceil(maxX)), range(floor(minY) + 1, ceil(maxY) + 1)
    )

    pair_to_name = (
        lambda lon, lat: f"n{lat}{f'w{str(lon)[1:].zfill(3)}' if lon < 0 else f'e{lon:03}'}"
    )
    name_to_url = lambda name, ext: f"{url_base}{name}/USGS_13_{name}.{ext}"

    tiles = {}
    for (lon, lat) in coord_pairs:
        name = pair_to_name(lon, lat)
        tiles[name] = {
            "url": name_to_url(name, "tif"),
            "metadata": name_to_url(name, "xml"),
        }

    return tiles


async def fetch_tile_metadata(tiles):
    results = {}
    async with aiohttp.ClientSession() as session:
        for tile_key in tiles:
            async with session.get(tiles[tile_key]["metadata"]) as resp:
                payload = await resp.text()
                results[tile_key] = xmltodict.parse(payload)

    return {k: {"url": v["url"], "metadata": results[k]} for k, v in tiles.items()}


def metadata_to_bbox(meta):
    bounds = meta["metadata"]["idinfo"]["spdom"]["bounding"]
    return [
        float(bounds["westbc"]),
        float(bounds["southbc"]),
        float(bounds["eastbc"]),
        float(bounds["northbc"]),
    ]


async def fetch_elevation(bbox, dst_crs=None, dst_transform=None, dst_shape=None):
    tile_urls = get_3dep_tile_urls(bbox)
    results = await asyncio.gather(
        *[fetch_raster_async(url, bbox=bbox) for url in tile_urls],
        return_exceptions=True,
    )
    good_results = [x for x in results if type(x) == tuple]
    exceptions = [x for x in results if isinstance(x, Exception)]
    if len(exceptions) > 0:
        raise exceptions[0]

    if len(set(ds[2] for ds in good_results)) > 1:
        raise ValueError("All rasters must be in the same CRS.")

    if any(
        elem is not None for elem in [dst_crs, dst_transform, dst_shape]
    ) and not all(elem is not None for elem in [dst_crs, dst_transform, dst_shape]):
        warnings.warn(
            "dst_crs, dst_transform, and dst_shape are all required for reprojection"
        )

    merged, merged_tf, merged_crs = merge_rasters(good_results)

    if dst_crs is not None and dst_transform is not None:
        with rio.Env():
            ele_reproj, ele_reproj_tf = rio.warp.reproject(
                merged,
                destination=zeros(dst_shape),
                src_transform=merged_tf,
                src_crs=merged_crs,
                dst_transform=dst_transform,
                dst_crs=dst_crs,
                dst_nodata=nan, # to avoid GDAL default of 0
                resampling=rio.warp.Resampling.nearest,
            )
        return ele_reproj, ele_reproj_tf, dst_crs

    return merged, merged_tf, merged_crs


# def fetch_elevation(bbox, dst_crs=None, dst_transform=None, dst_shape=None):
#     tile_urls = get_3dep_tile_urls(bbox)
#     # TODO: fetch all tiles and build VRT, right now just fetching first tile
#     # see https://github.com/cheginit/pygeoutils/blob/14237fdd2684b15cf4cba05752c7b4a699d261ad/pygeoutils/pygeoutils.py#L359
#     with rio.open(tile_urls[0]) as src:
#         window = rio.windows.from_bounds(*bbox, transform=src.transform)
#         transform = rio.windows.transform(window, src.transform)
#         crs = src.crs
#         elev = src.read(1, window=window)

#     if dst_crs is None and dst_transform is None and dst_shape is None:
#         return elev, transform

#     with rio.Env():
#         ele_reproj, ele_reproj_tf = rio.warp.reproject(
#             elev,
#             destination=zeros(dst_shape),
#             src_transform=transform,
#             src_crs=crs,
#             dst_transform=dst_transform,
#             dst_crs=dst_crs,
#             resampling=rio.warp.Resampling.nearest,
#         )
#         return ele_reproj, ele_reproj_tf

def calculate_slope(elev):
    mg = RasterModelGrid(elev.shape)
    z = elev.flatten()
    return mg.calc_slope_at_node(elevs=z).reshape(elev.shape)


def calculate_aspect(elev):
    mg = RasterModelGrid(elev.shape)
    z = elev.flatten()
    return mg.calc_aspect_at_node(elevs=z).reshape(elev.shape)
