from bson.objectid import ObjectId
import shapely
import datetime
import json

def load_fields_from_projects_file(path):
    """
    Opens a project file and returns the fields

    Parameters:
        path: string, required
            a path to the desired file to load. The specified
            file should be JSON containing an array of projects
            e.g. projects = [
                {
                    producers: [
                        {
                            fields: [
                                {
                                    "boundaryGeoJSON": {
                                        # ...
                                    }
                                    # ...
                                }
                            ]
                        }
                    ]
                }
            ]
    """
    with open(path, "r") as f:
        projects = json.load(f)

    fields = []
    for project in projects:
        for producer in project["producers"]:
            fields.extend(producer["fields"])

    return fields


def fields_dict_by_human_id(fields):
	return { field['fieldByProjectId']: field for field in fields}


def create_field(
	areaIds, 
	xid=None,
	name='Field', 
	contactPoint=[],
):
	if xid is None:
		xid = str(ObjectId())

	return {
		'address': {},
		'contactPoint': contactPoint,
		'area': areaIds,
		'name': name,
		'id': xid,
	}


def create_area(
	boundary, 
	xid=None,
	name='Field Boundary', 
	field_id=None, 
	creator=None,
):
	if not shapely.geometry.shape(boundary).is_valid or (boundary['type'] not in ['Polygon', 'MultiPolygon']):
		raise ValueError('invalid boundary')

	if xid is None:
		xid = str(ObjectId())

	return {
		'id': xid,
		'geometry': boundary,
		'properties': {
			'name': name,
			'featureOfInterest': field_id,
			'dateCreated': datetime.datetime.utcnow().replace(microsecond=0).isoformat(),
			'creator': creator,
		},
		'type': 'Feature',
	}


def create_stratification(
	# location_collection_id=None,
	area_id=None,
	xid=None,
	name="Stratification",
	algorithm_version='0.1.0',
	agent=None,
	random_seed=None,
	clhs_max_iterations=1000,
	number_of_clusters=None,
	sample_density=None,
):
	if xid is None:
		xid = str(ObjectId())

	return {
		'id': xid,
		# 'object': location_collection_id,
		'object': area_id,
		'name': name,
		'agent': agent,
		'dateCreated': datetime.datetime.utcnow().replace(microsecond=0).isoformat(),
		'algorithm': {
			'name': 'OUR_SCI_STRATIFICATION',
			'alternateName': 'SKLEARN_KMEANS_RANGE_CLHS',
			# codeRepository?: string;
			'version': '0.1.0',
			# doi?: string;
		},
		'provider': 'OUR_SCI',
		'input': [
			{
				'name': 'INPUT_LAYER_SENTINEL_2_L2A',
				'value': 'https://sentinel.esa.int/web/sentinel/user-guides/sentinel-2-msi/product-types/level-2a',
			},
			{
				'name': 'INPUT_LAYER_3DEP',
				'value': 'https://www.usgs.gov/core-science-systems/ngp/3dep',
			},
			{
				'name': 'INPUT_LAYER_SSURGO',
				'value': 'https://www.nrcs.usda.gov/wps/portal/nrcs/detail/soils/survey/?cid=nrcs142p2_053627',
			},
			{
				'name': 'RANDOM_SEED',
				'value': random_seed,
			},
			{
				'name': 'CLHS_MAX_ITERATIONS',
				'value': clhs_max_iterations,
			},
			{
				'name': 'CLUSTERING_NUMBER_OF_CLUSTERS',
				'value': number_of_clusters,
			},
			{
				'name': 'SAMPLE_DENSITY_SAMPLES_PER_ACRE',
				'value': sample_density,
			},
		],
		'result': [

		]
	}


def create_location_collection(
	xid=None,
	stratification_id=None,
	area_id=None,
	field_id=None,
	locations=[],
):
	if xid is None:
		xid = str(ObjectId())

	return {
		'id': xid,
		'resultOf': stratification_id,
		'featureOfInterest': field_id,
		'object': area_id,
		'features': locations,
	}


def create_location(
	point_coords,
	xid=None,
	stratum=None,
):
	if xid is None:
		xid = str(ObjectId())

	return {
		'id': xid,
		'type': 'Feature',
		'properties': {
			'stratum': stratum,
		},
		'geometry': {
			'type': 'Point',
			'coordinates': point_coords,
		},
	}



def create_export_file(
	location_coords, 
	boundary, 
	name=None, 
	stratification_args={},
	location_strata=[]
):
	if name is None:
		name = datetime.datetime.utcnow().replace(microsecond=0).isoformat().replace(':', '_')

	# locations = [create_location(c, xid=str(ObjectId())) for c in location_coords]

	if len(location_strata) > 0 and len(location_strata) != len(location_coords):
		raise ValueError('location_strata does not match location_coords')

	if len(location_strata) > 0:
		locations = [
			create_location(c, stratum=s) 
			for c, s in zip(location_coords, location_strata)
		]
	else: 
		locations = [create_location(c, xid=str(ObjectId())) for c in location_coords]
		



	# location_collection_id = str(ObjectId())
	# field_id = str(ObjectId())
	# area = create_area(boundary, field_id=field_id)
	# field = create_field([area['id']], xid=field_id)
	area = create_area(boundary, name=f"Field {name} Area")
	field = create_field([area['id']], name=f"Field {name}")
	stratification = create_stratification(
		area_id=area['id'],
		name=f"Field {name} Stratification",
		**stratification_args,
	)
	location_collection = create_location_collection(
		stratification_id=stratification['id'],
		field_id=field['id'],
		area_id=area['id'],
		locations=[loc['id'] for loc in locations],
	)
	return json.dumps({
		'areas': [area],
		'fields': [field],
		'stratifications': [stratification],
		'locations': locations,
		'locationCollections': [location_collection],
	})

